let express = require('express')
let app = express()
let hbs = require('express-handlebars')
let bodyParser = require('body-parser')
let passport = require('passport')
let session = require('express-session')
let LocalStrategy = require('passport-local').Strategy
let mongoose = require('mongoose')
let logger = require('morgan')
let path = require('path')
let errorHandler = require('errorhandler')
let cookieParser = require('cookie-parser')

let favicon = require('serve-favicon')
let Account = require('./models/Account')
let routes = require('./routes/index')
const config = require('./config')
const port = 3000

mongoose.connect(config.DBhost, {useNewUrlParser: true, autoIndex: false })
	.then(err => console.log(`Successfully connected to database : ${DB_URI}`))
	.catch(err => console.error.bind(console, 'connection error:'))


app.set('view engine', 'hbs')
app.engine('hbs', hbs({defaultLayout: 'layout', extname: 'hbs'}))

app.use(logger('dev'))
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ type: 'application/json' }))
app.use(express.static(path.join( __dirname, 'public')))
app.use(favicon(`${__dirname}/public/favicon.ico`))
app.use(session({
	cookieName: 'session',
	secret: 'fonciafonciafoncia',
	resave: false,
	saveUninitialized: false
}))

if ('development' === app.get('env')) app.use(errorHandler())
app.use(passport.initialize())
app.use(passport.session())

passport.use(new LocalStrategy(Account.authenticate()))
passport.serializeUser(Account.serializeUser())
passport.deserializeUser(Account.deserializeUser())

app.use('/', routes)

app.use((req, res, next) => {
	let err = new Error('Not Found')
	err.status = 404
	next(err)
})

if (app.get('env') === 'development') {
	app.use((err, req, res, next) => {
		res.status(err.status || 500)
		res.render('error' , {
			message: err.message,
			error: err
		})
	})
}

app.use((err, req, res, next) => {
	res.status(err.status || 500)
	res.render('error' , {
		message: err.message,
		error: {}
	})
})

if (!module.parent) app.listen(port, () => console.log(`App listening on port ${port}`))
module.exports = app
