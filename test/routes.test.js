let mongoose = require('mongoose')
let Client = require('../models/Client')

let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../server')
let should = chai.should()

chai.use(chaiHttp)

describe('Routes', () => {
	describe('/GET clients', () => {
		it('it should get all the client', (done) => {
			chai.request(server)
				.get('/api/foncia/clients')
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a('array')
					res.body.length.should.be.gt(0)
					done()
				})
		})
	})

	describe('/GET lots', () => {
		it("it should return a status 404 when idClient missing", (done) => {
			chai.request(server)
				.get(`/api/foncia/client/`)
				.end((err, res) => {
					res.should.have.status(404)
					done()
				})
		})

		it("it should return a status 400 when idClient isn't good", (done) => {
			const idClient = 'notValide'

			chai.request(server)
				.get(`/api/foncia/client/${idClient}`)
				.end((err, res) => {
					res.should.have.status(400)
					done()
				})
		})

		it('it should get all lots for a client', (done) => {
			const idClient = '5be0e99d9228d14c03dae2f7'
			chai.request(server)
				.get(`/api/foncia/client/${idClient}`)
				.end((err, res) => {
					res.should.have.status(200)
					res.body.length.should.be.gt(0)
					done()
				})
		})
	})

	describe('/POST client', () => {
		it("it should save a client", (done) => {
			let client = {
				fullname: 'LastName FirstName',
				email: 'email@email.com'
			}

			chai.request(server)
				.post(`/api/foncia/client/`)
				.send(client)
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.have.property('message').eql('Client successfully added!')
					done()
				})
		})
	})

	describe('/POST lot', () => {
		it("it should save a lot", (done) => {
			let lot = {
				client: '5be0e99d9228d14c03dae2f7',
				surface: '20'
			}

			chai.request(server)
				.post(`/api/foncia/lot/`)
				.send(lot)
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.have.property('message').eql('Lot successfully added!')
					done()
				})
		})
	})
})
