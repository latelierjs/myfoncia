# mongoose-test [![NPM version](https://badge.fury.io/js/mongoose-test.svg)](https://npmjs.org/package/mongoose-test) [![Build Status](https://travis-ci.org/latelierjs/mongoose-test.svg?branch=master)](https://travis-ci.org/latelierjs/mongoose-test)

> MyFoncia API,
Il est necessaire d'avoir une DB mongo qui tourne sur la machine
Les exports de la base se trouve dans /dump
Sans authentification vous serez redirigé vers la page /login


## Installation

```sh
$ npm i
$ npm run start

```

## test
```sh
$ make test
```

## Liens
/

## License

ISC © [Ngeke Mongi]()
