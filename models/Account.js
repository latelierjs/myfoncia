//let passport = require('passport')
let mongoose = require('mongoose')
let Schema = mongoose.Schema
//let crypto = require('crypto')
let passportLocalMongoose = require('passport-local-mongoose')


let Account = new Schema({
	username: String,
	password: String
})

/*Account.pre('save', (next) => {
	if (this.password) {
		this.salt = new Buffer(
			crypto.randomBytes(16).toString('base64'),
			'base64'
		)

		this.password = crypto.pbkdf2Sync(
			password,
			this.salt,
			10000,
			64
		).toString('base64')
	}

	next()
})*/

Account.plugin(passportLocalMongoose)
module.exports = mongoose.model('Account', Account)
