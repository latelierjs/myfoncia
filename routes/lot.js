let mongoose = require('mongoose')
let Lot = require('../models/Lot')

function getLotsByClient(req, res) {
	Lot
		.find({ client: req.params.id })
		.populate('client')
	 	.exec((err, lot) => {
			if (err) {
				res.status(400)
				return res.json(err)
			}
			else res.json(lot)
		})
}

function postLot(req, res) {
	new Lot(req.body)
		.save((err, lot) => {
			if (err) res.send(err)
			else res.json({message: 'Lot successfully added!', lot })
		})
}

module.exports = { getLotsByClient, postLot }
