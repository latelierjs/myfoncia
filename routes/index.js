let router = require('express').Router()
let passport = require('passport')
let client = require('./client')
let fetch = require('node-fetch')
let Account = require('../models/Account')
let lot = require('./lot')

const config = require('../config')

router.get(`${config.prefixApiUrl}/clients`, client.getClients)
router.post(`${config.prefixApiUrl}/client`, client.postClient)
router.get(`${config.prefixApiUrl}/client/:id`, lot.getLotsByClient)
router.post(`${config.prefixApiUrl}/lot`, lot.postLot)

router.get('/', (req, res) => {
	if (!req.user || !req.user.username) return res.redirect('/login')

	fetch(`http://localhost:3000/api/foncia/clients`)
		.then(response => response.json())
		.then(clients => res.render('index', {clients: clients, username: req.user.username }))
		.catch(err => console.log(err))
})
router.get('/client/:id', (req, res) => {
	const {id} = req.params
	fetch(`http://localhost:3000/api/foncia/client/${id}`)
		.then(response => {
			return response.json()
		})
		.then(lots => {
			let [lot] = lots
			res.render('client', {client: lot.client, lots: lots, nFlat: lots.length })
		})
		.catch(err => console.log(err))
})

router.get('/register', (req, res) => {
	res.render('register')
})
router.post('/register', (req, res) => {
	Account.register(
		new Account({username: req.body.username}),
		req.body.password,
		(err, account) => {
			if (err) return res.render('register', { error: err.message })
			passport.authenticate('local')(req, res, () => res.redirect('/'))
		}
	)
})
router.get('/login', (req, res) => {
	if (req.user && req.user.username) return res.redirect('/')
	res.render('login')
})

router.post('/login', passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login'}))

router.get('/error', (req, res) => res.render('error'))
module.exports = router
