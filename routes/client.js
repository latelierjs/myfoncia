let mongoose = require('mongoose')
let Client = require('../models/Client')

function getClients(req, res) {
	Client
		.find()
		.exec((err, clients) => {
			if (err) throw err
			res.json(clients)
		})
}

function postClient(req, res) {
	new Client(req.body)
		.save((err, client) => {
			if (err) throw err
			else res.json({message: 'Client successfully added!', client })
		})
}

module.exports = { getClients, postClient }
